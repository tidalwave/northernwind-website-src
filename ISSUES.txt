Publish a blog about the new XSLT macro for StoppingDown.




* NWW-1: Explain Resource Properties

* NWW-2: Explain Nodes.

* NWW-3: Add a Getting started section. It must be possible to locally run a simple setup with a few passages:
	+ Downloading a single POM with curl that can be launched with mvn jetty:run
	+ Launching a Java Web Start link.
	+ Unpacking a .zip file and running the standalone version with a command line pointing to the unpacked site.
        Depends on NW-5.

* NWW-5: Prepare a copy of the website as a sample website. (use the archetype instead).

* NWW-6: Add a blog post to explain how to use BitBucket as a repository (in the deployment category).


* Blog posts todo: AvailabilityEnforcer, NorthernWind Resources, Deploying to Jetty, FLOSS Projects Macros, CachedUriResolver


CLOSED, FIXED

* NWW-4: Add a Download section.

* NWW-7: Fix Blog Sidebar.

* NWW-8: Create Blog categories: features, miscellaneous, development, deployment.

