
<xsl:template match="div[@class='nwXsltMacro_NorthernWindExample']">
    <xsl:variable name="baseUrl" select="p[@class='nwXsltMacro_NorthernWindExample_url']"/>
    <xsl:variable name="componentUrl">
        <xsl:value-of select="$baseUrl"/>
        <xsl:value-of select="'/Components_en.xml'"/>
    </xsl:variable>
    <xsl:variable name="propertyUrl">
        <xsl:value-of select="$baseUrl"/>
        <xsl:value-of select="'/Properties_en.xml'"/>
    </xsl:variable>
    <xsl:element name="div">
        <xsl:attribute name="class">nw-example-components</xsl:attribute>
        <xsl:element name="div">
            <xsl:attribute name="class">nw-example-component</xsl:attribute>
            <xsl:element name="div">
                <xsl:attribute name="class">component-header</xsl:attribute>
                <xsl:text>Global properties</xsl:text>
            </xsl:element>
            <xsl:for-each select="document($propertyUrl)/properties">
                <xsl:call-template name="process-properties">
                    <xsl:with-param name="properties" select="."/>
                </xsl:call-template>
            </xsl:for-each>
        </xsl:element>
        <xsl:for-each select="document($componentUrl)/components/component">
            <xsl:call-template name="process-component">
                <xsl:with-param name="component" select="."/>
                <xsl:with-param name="propertyUrl" select="$propertyUrl"/>
            </xsl:call-template>
        </xsl:for-each>
    </xsl:element>
</xsl:template>

<xsl:template name="process-component">
    <xsl:param name="component"/>
    <xsl:param name="propertyUrl"/>

    <xsl:text>&#10;</xsl:text>
    <xsl:variable name="componentId" select="$component/@id"/>
    <xsl:element name="div">
        <xsl:attribute name="class">nw-example-component</xsl:attribute>
        <xsl:element name="div">
            <xsl:attribute name="class">component-header</xsl:attribute>
            <xsl:value-of select="$componentId"/>
            <xsl:text> (</xsl:text>
            <xsl:value-of select="substring-after($component/@type, 'http://northernwind.tidalwave.it/component/')"/>
            <xsl:text>)</xsl:text>
        </xsl:element>
        <xsl:for-each select="document($propertyUrl)/properties/properties[@id=$componentId]">
            <xsl:call-template name="process-properties">
                <xsl:with-param name="properties" select="."/>
            </xsl:call-template>
        </xsl:for-each>
        <xsl:for-each select="$component/component">
            <xsl:call-template name="process-component">
                <xsl:with-param name="component" select="."/>
                <xsl:with-param name="propertyUrl" select="$propertyUrl"/>
            </xsl:call-template>
        </xsl:for-each>
    </xsl:element>
    <xsl:text>&#10;</xsl:text>
</xsl:template>

<xsl:template name="process-properties">
    <xsl:param name="properties"/>
    
    <xsl:element name="table">
        <xsl:for-each select="$properties/property">
            <xsl:element name="tr">
                <xsl:text>&#10;</xsl:text>
                <xsl:element name="td">
                    <xsl:attribute name="class">property-name</xsl:attribute>
                    <xsl:value-of select="@name"/>
                    <xsl:text>:</xsl:text>
                </xsl:element>
                <xsl:element name="td">
                    <xsl:attribute name="class">property-value</xsl:attribute>
                    <xsl:for-each select="values/value">
                        <xsl:value-of select="."/>
                        <xsl:element name="br"/>
                        <xsl:text>&#10;</xsl:text>
                    </xsl:for-each>
                    <xsl:for-each select="value">
                        <xsl:value-of select="."/>
                    </xsl:for-each>
                </xsl:element>
            </xsl:element>
        </xsl:for-each>
    </xsl:element>
</xsl:template>